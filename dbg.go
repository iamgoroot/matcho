package matcho

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
)

func (m Matcho) Dbg() Matcho {
	dbg := fmt.Sprint(
		"\n\n=============================\n",
		m.Slice(20, 0),
		"<Index[", m.Index, "]>",
		m.Slice(0, 20),
		"\n=============================\n",
	)
	log.Println("{\n Dbg:\n`", dbg, "`, Index:[", m.Index, "], Index", m.Index, ", Op:", m.OpStr(), ", ErrorStr: "+m.ErrorStr+"}\n\n")
	return m.Move(0)
}

func (m Matcho) CurrentLine() string {
	return string(m.Source[m.Rewind("\n").Pos():m.Next("\n").Pos()])
}

func (m Matcho) CurrentLineDebug() string {
	start := m.Rewind("\n").Pos()
	end := m.Next("\n").Pos()
	builder := strings.Builder{}
	builder.WriteRune('\n')
	builder.WriteString(strings.Repeat("=", 16))
	builder.WriteRune('\n')
	builder.Write(m.Source[start:m.Index])
	builder.WriteString("<Index:")
	builder.WriteString(fmt.Sprint(m.Index))
	builder.WriteRune('>')
	builder.Write(m.Source[m.Index:end])
	builder.WriteRune('\n')
	builder.WriteString(strings.Repeat("=", 16))
	builder.WriteRune('\n')

	return builder.String()
}

func (m Matcho) StringDebug() string {
	builder := strings.Builder{}
	builder.WriteString(`{"`)
	builder.Write(m.Source)
	builder.WriteString(`", `)
	builder.WriteString(fmt.Sprint(m.Index))
	if m.ErrorStr != "" {
		builder.WriteRune(',')
		builder.WriteString(m.ErrorStr)
	}
	builder.WriteRune('}')
	return builder.String()
}

var debug int

type DebugInfo struct {
	From     int
	To       int
	Data     string //more readable json
	OpResult string
	Error    string

	StackTrace string
}

func event(m Matcho, to int) {
	var pcs [5]uintptr
	n := runtime.Callers(3, pcs[:])
	iter := runtime.CallersFrames(pcs[:n])
	var file, function string
	line := 0
	stack := strings.Builder{}
	for {
		f, more := iter.Next()
		if !more {
			break
		}
		file = f.File
		line = f.Line
		function = f.Function
		if len(file) > 30 {
			stack.WriteString("...")
			stack.WriteString(file[30:])
		} else {
			stack.WriteString(file)
		}
		stack.WriteRune(':')
		stack.WriteString(strconv.Itoa(line))
		stack.WriteRune(' ')
		stack.WriteString(filepath.Base(function))
		stack.WriteRune('\n')

	}

	str := m.String()
	m.Source = nil
	body, _ := json.Marshal(DebugInfo{m.Index, to, str, m.OpStr(), m.Err(), stack.String()})
	http.Post(fmt.Sprint("http://localhost:", debug, "/debug"), "application/json", bytes.NewBuffer(body))
}

func DebugOnPort(p int) {
	debug = p
}
