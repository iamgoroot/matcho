# Matcho 	[![Go Report Card](https://goreportcard.com/badge/gitlab.com/iamgoroot/matcho)](https://goreportcard.com/report/gitlab.com/iamgoroot/matcho)	[![Build Status](https://travis-ci.org/iamgoroot/matcho.svg?branch=master)](https://travis-ci.org/iamgoroot/matcho)
> Text processing index abstraction and helpers

## Motivation

It was supposed to be a simple parsing job and then it grew up into loopy-checkindex-check-error-regexy stuff? 

Now there is one more thing you can throw in
 
## Installation

```
go get gitlab.com/iamgoroot/matcho
```

## What can (possibly?) it do

So far I used it to parse imports in go files and there are some examples looking up property files and json (not that you have to make json parser out of it.)

It's nice if you need to read that one specific thing out of text (for now only text) but really want to struggle (should i say "less" or "more"? let me know)

See test package for examples.

## Basics

First of all: There is no silver bullet. If you better of parsing without Matcho you can grab all the state:

```
type Matcho struct {
	Source   []byte	//Your string or data
	Index    int	//Position you are in
	ErrorStr string		//Last error string
	OperationResult []byte	//Last "Select" operation result
}
```

Wrap your with matcho.Of([]byte) or matcho.OfStr(string) and you are ready to go

[See Method reference](reference/interface.go)

## Usage example

See test package for more examples

[Examples basic](test/examples1_test.go)
[Examples looping](test/examples2_test.go)
[Parsing specific part of json array](test/bench_test.go)
[External tool to parse imports](test/imports_test.go)


## Debugging

It has debugging interface with "new year mode" just do matcho.DebugOnPort(int) and run app in [debug](/debug) folder

[![IMAGE ALT TEXT](http://img.youtube.com/vi/7Ej41ygiYdA/0.jpg)](https://www.youtube.com/watch?v=7Ej41ygiYdA "Matcho debug new year mode")

## Contributing

Feel free to contribure with code or feedback
