package matcho

import (
	"bytes"
	"strconv"
)

type Matcho struct {
	Source          []byte //Your string or data
	Index           int    //Position you are in
	ErrorStr        string //Last error string
	OperationResult []byte //Last "Select" operation result
}

func OfStr(string string) Matcho {
	return Matcho{Source: []byte(string)}
}

func Of(bytes []byte) Matcho {
	return Matcho{Source: bytes}
}


func (m Matcho) Len() int {
	return len(m.Source)
}
func (m Matcho) String() string {
	return string(m.Source)
}

func (m Matcho) Before(token string) Matcho {
	cut := m.Rest()
	index := bytes.Index(cut, []byte(token))
	if index < 0 {
		return m.Fail("failed next", token, "at", strconv.Itoa(m.Index), "index", strconv.Itoa(index))
	}

	return m.Move(index)
}

func (m Matcho) Next(after string) Matcho {
	return m.Before(after).Move(len(after))
}

//Sets Op
//TODO: make faster?
func (m Matcho) NextAny(any ...string) Matcho {
	runes := make([]byte, len(any))
	for i, str := range any {
		runes[i] = str[0]
	}
	off := 0
	rest := m.Rest()
	for {
		i := bytes.IndexAny(rest, string(runes))
		if i < 0 {
			break
		}
		rr := runes

		l2: for {
			c := bytes.IndexByte(rr, rest[i])
			if c < 0 {
				break l2
			}
			check := any[c]
			l := len(check)

			text := rest[i:i+l]
			if check == string(text) {
				return m.OpSet(text).Move(off+i + l)
			}
			rr = rr[c +1:]
		}
		off += i
		rest = rest[i + 1:]
	}
	return m.FailEof("not found any")
}

func (m Matcho) Rewind(until string) Matcho {
	cut := m.Source[:m.Index]
	index := bytes.LastIndex(cut, []byte(until))
	if index < 0 {
		index = 0
	}
	return m.Move(-m.Index + index)
}
