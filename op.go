package matcho

//Returns Matcho for last operation retult
func (m Matcho) Op() Matcho {
	return Matcho{Source: m.OperationResult}
}

//OpStr last operation retult
func (m Matcho) OpStr() string {
	return string(m.OperationResult)
}

//OpBytes last operation retult
func (m Matcho) OpBytes() []byte {
	return m.OperationResult
}

//Set last operation retult
func (m Matcho) OpSet(result []byte) Matcho {
	m.OperationResult = result
	return m
}

//Set last operation retult
func (m Matcho) OpNil() Matcho {
	m.OperationResult = nil
	return m
}
