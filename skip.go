package matcho

import (
	"fmt"
	"strings"
)

func (m Matcho) Skip(anyOf string) Matcho {
	for ; m.Index < len(m.Source) && strings.ContainsRune(anyOf, rune(m.Source[m.Index])); m.Index++ {
	}
	return m
}

//
//func (m Matcho) SkipBack(anyOf string) Matcho {
//	_, mm := m.SelectVar(anyOf)
//	return mm
//}

func (m Matcho) Indent() Matcho {
	return m.Skip(" \n\r\t")
}
func (m Matcho) Sp() Matcho {
	mm := m.Skip(" \t")
	if mm.Pos() == m.Index {
		return m.Fail("missing space " + mm.SliceCombine(20, fmt.Sprint("post[", m.Index, "]"), 20).String())
	}
	return mm
}

//space maybe
func (m Matcho) SpMb() Matcho {
	return m.Skip(" \t")
}

func (m Matcho) SkipSeq(tokens ...string) Matcho {
	for _, t := range tokens {
		m.Next(t)
	}
	return m
}
