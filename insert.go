package matcho

import (
	"bytes"
	"sort"
)

//Probably breaks all other Matchos
func (m Matcho) Insert(s string) Matcho {
	return JoinBytes(m.Source[:m.Index], []byte(s), m.Source[m.Index:])
}

//Makes new backing slice
func (m Matcho) InsertAll(points ...InsertPoint) Matcho {
	pts := insertPoints(points)
	sort.Stable(pts)

	buf := bytes.Buffer{}
	lastIndex := 0
	for _, p := range pts {
		if lastIndex < p.Matcho.Index {
			buf.Write(m.Source[lastIndex:p.Matcho.Index])
		}
		lastIndex = p.Matcho.Index
		buf.WriteString(p.Value)
	}
	if lastIndex < len(m.Source) {
		buf.Write(m.Source[lastIndex:len(m.Source)])
	}
	return Matcho{Source: buf.Bytes()}
}

func (m Matcho) ReplaceAll(old, new string) Matcho {
	return Matcho{Source: bytes.Replace(m.Source, []byte(old), []byte(new), -1)}
}

func (m Matcho) InsertPoint(s string) InsertPoint {
	return InsertPoint{m, s}
}

type insertPoints []InsertPoint
type InsertPoint struct {
	Matcho Matcho
	Value  string
}

func (p insertPoints) Len() int {
	return len(p)
}

func (p insertPoints) Less(i, j int) bool {
	return p[i].Matcho.Index < p[j].Matcho.Index
}

func (p insertPoints) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}
