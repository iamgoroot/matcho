package matcho

import (
	"bytes"
	"regexp/syntax"
	"strings"
)

func (m Matcho) Select(until string) Matcho {
	cut := m.Rest()
	index := bytes.Index(cut, []byte(until))
	if index < 0 {
		index = len(cut)
	}
	moved := m.Move(index)
	op := cut[:index]
	moved.OperationResult = op
	return moved
}

//moves. get result matcho.SelectVar().Op()
func (m Matcho) SelectVar(allowed string) Matcho {
	f := func(r rune) bool {
		isWord := syntax.IsWordChar(r)
		isAllowed := strings.ContainsRune(allowed, r)
		return !(isWord || isAllowed)
	}
	moved := m.SelectVarFunc(f)
	return moved
}

//not moves
func (m Matcho) SelectVal(allowed string) string {
	return m.SelectVar(allowed).OpStr()
}

func (m Matcho) SelectVarFunc(f func(r rune) bool) Matcho {
	cut := m.Rest()
	index := bytes.IndexFunc(cut, f)
	if index < 0 {
		index = len(cut)
	}
	return m.OpSet(cut[:index]).Move(index)
}

func (m Matcho) SelectVarInto(allowed string, v *string) Matcho {
	mm := m.SelectVar(allowed)
	str := mm.OpStr()
	*v = str
	return mm
}

func (m Matcho) SelectVarUntil(v *string, until string) Matcho {
	mm := m.Select(until)
	str := mm.OpStr()
	*v = str
	return mm
}
