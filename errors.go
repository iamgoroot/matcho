package matcho

import "strings"

//Moves received value one position right
func (m Matcho) OnErr(matcho Matcho) Matcho {
	if m.ErrorStr != "" {
		return matcho
	}
	return m
}

func (m Matcho) OnErrStep(matcho Matcho) Matcho {
	if m.ErrorStr != "" {
		return matcho.Move(1)
	}
	return m
}

func (m Matcho) OnOk(matcho Matcho) Matcho {
	if m.ErrorStr == "" {
		return matcho
	}
	return m
}

func (m Matcho) On(condition bool, matcho Matcho) Matcho {
	if condition {
		return matcho
	}
	return m
}

func (m Matcho) OnOkOpStr(op *string) Matcho {
	if m.ErrorStr == "" {
		*op = m.OpStr()
	}
	return m
}

func (m Matcho) OnErrCall(f func(Matcho) Matcho) Matcho {
	if m.ErrorStr != "" {
		return f(m)
	}
	return m
}
func (m Matcho) OnOkCall(f func(Matcho) Matcho) Matcho {
	if m.ErrorStr == "" {
		return f(m)
	}
	return m
}
func (m Matcho) OnEof(matcho Matcho) Matcho {
	if m.Eof() {
		return matcho
	}
	return m
}

//Err
func (m Matcho) Err() string {
	return m.ErrorStr
}

//Err
func (m Matcho) ErrSet(prt *string) Matcho {
	*prt = m.ErrorStr
	return m
}

//Zero on error
func (m Matcho) Zoe() Matcho {
	if m.ErrorStr != "" {
		return Matcho{}
	}
	return m
}

//End Of File
func (m Matcho) Eof() bool {
	return m.Index >= len(m.Source) || m.Index < 0
}

//Panic Nn ErrorStr
func (m Matcho) Pof() Matcho {
	if m.ErrorStr != "" {
		panic(m.ErrorStr)
	}
	return m
}

//End Of File
func (m Matcho) ErrReset() Matcho {
	m.ErrorStr = ""
	return m
}

func (m Matcho) Reset() Matcho {
	m.Index = 0
	m.ErrorStr = ""
	m.OperationResult = nil
	return m
}

func (m Matcho) Fail(err ...string) Matcho {
	m.ErrorStr = strings.Join(err, "\n")
	//m.ErrorStr = "1"

	return m
}

func (m Matcho) FailEof(err ...string) Matcho {
	m.Index = len(m.Source)
	m.ErrorStr = strings.Join(err, "\n")
	//m.ErrorStr = "1"
	return m
}

func errToStr(err error) string {
	if err != nil {
		return err.Error()
	}
	return ""
}
