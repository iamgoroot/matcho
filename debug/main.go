package main

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"sync"

	"github.com/gorilla/mux"
	"gitlab.com/iamgoroot/matcho"
)

var matchos = map[string]Json{}
var mut = sync.RWMutex{}

type Json struct {
	Data    string
	Session string
	Info    []matcho.DebugInfo
}

func main() {
	f := func(writer http.ResponseWriter, request *http.Request) {

		info := matcho.DebugInfo{}
		if json.NewDecoder(request.Body).Decode(&info) != nil {
			return
		}

		sum := md5.Sum([]byte(info.Data))
		hash := fmt.Sprintf("%x", sum)
		mut.Lock()
		defer mut.Unlock()
		states := matchos[hash]
		states.Data = info.Data
		states.Session = hash
		states.Info = append(matchos[hash].Info, info)
		matchos[hash] = states
	}

	f2 := func(writer http.ResponseWriter, request *http.Request) {
		mut.RLock()
		defer mut.RUnlock()
		bytes, _ := json.Marshal(matchos)
		writer.Write(bytes)
	}

	f3 := func(writer http.ResponseWriter, request *http.Request) {
		sum := strings.TrimSpace(request.FormValue("sum"))
		info := matchos[sum]
		if info.Data == "" {
			writer.WriteHeader(http.StatusNotFound)
			return
		}
		var inserts []matcho.InsertPoint
		for _, value := range info.Info {
			color := fmt.Sprintf("rgb(%d,%d,%d)", rand.Intn(200)+20,
				rand.Intn(200)+20,
				rand.Intn(200)+20)
			m := matcho.Matcho{Source: []byte(info.Data), Index: value.From}
			start := matcho.InsertPoint{
				Matcho: m,
				Value: fmt.Sprintf(
					`<a class="position" style="background-color:%s">%d<span class="hidden">
<pre>
Start of move to %d
Having operation result: %s
Having Error: %s
Stack trace:
%s</pre>
</span></a>`,
					color,
					value.From,
					value.To,
					value.OpResult,
					value.Error,
					value.StackTrace,
				),
			}
			m = matcho.Matcho{Source: []byte(info.Data), Index: value.To}
			end := matcho.InsertPoint{
				Matcho: m,
				Value: fmt.Sprintf(
					`<a class="position" style="background-color:%s">%d<span class="hidden">
<pre>
End of move from %d
Having operation result: %s
Having Error: %s
Stack trace:
%s</pre>
</span></a>`,
					color,
					value.To,
					value.From,
					value.OpResult,
					value.Error,
					value.StackTrace,
				),
			}
			inserts = append(inserts, start, end)
		}
		m := matcho.Matcho{Source: []byte(info.Data)}.InsertAll(inserts...)
		writer.Write(m.Source)

	}

	r := mux.NewRouter()
	r.HandleFunc("/debug", f)
	r.HandleFunc("/clear", func(writer http.ResponseWriter, request *http.Request) {
		matchos = map[string]Json{}
	})

	r.HandleFunc("/read", f2)
	r.HandleFunc("/session", f3)
	r.PathPrefix("/lib/").Handler(http.StripPrefix("/lib/", http.FileServer(http.Dir("./ui/lib"))))
	//r.PathPrefix("/").Handler( http.FileServer(http.Dir("./ui")))
	r.PathPrefix("/css/").Handler(http.StripPrefix("/css/", http.FileServer(http.Dir("./ui/css"))))

	r.Handle("/", http.FileServer(http.Dir("./ui")))

	err := http.ListenAndServe(":59456", r)
	if err != nil {
		log.Fatal(err)
	}
}
