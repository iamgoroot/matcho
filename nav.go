package matcho

func (m Matcho) Pos() int {
	return m.Index
}

func (m Matcho) Move(off int) Matcho {
	to := m.Index + off
	if to < 0 {
		to = 0
	}
	if to > len(m.Source) {
		to = len(m.Source)
	}
	if debug > 80 {
		event(m, to)
	}
	m.Index = to
	return m
}

func (m Matcho) MoveTo(to Matcho) Matcho {
	m.Index = to.Index
	return m
}
