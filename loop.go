package matcho

//Loop Success (no fails) or EOF
func (m Matcho) UntilPass(repeat func(m Matcho) Matcho) Matcho {
	var matcho = m
	for !matcho.Eof() {
		sub := repeat(matcho)
		if sub.Err() == "" {
			return sub.Move(1)
		}
		matcho = sub
	}
	return m
}

func (m Matcho) UntilFail(repeat func(m Matcho) Matcho) Matcho {
	var matcho = m
	for !matcho.Eof() {
		sub := repeat(matcho)
		if sub.Err() != "" {
			return sub.Move(1)
		}
		matcho = sub
	}
	return m
}

//Loop Success (no fails) or EOF
func (m Matcho) UntilEof(repeat func(m Matcho) Matcho) Matcho {
	var matcho = m
	for !matcho.Eof() {
		matcho = repeat(matcho)
	}
	return matcho
}
