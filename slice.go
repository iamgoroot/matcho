package matcho

func (m Matcho) Slice(beforeCount, afterCount int) Matcho {
	start := m.Index - beforeCount
	end := m.Index + afterCount
	if start < 0 {
		start = 0
	}
	if end > len(m.Source) {
		end = len(m.Source)
	}

	return Matcho{Source: m.Source[start:end]}
}

func (m Matcho) Cut(start, end Matcho) Matcho {
	if start.Index < 0 {
		start.Index = 0
	}
	if end.Eof() {
		end.Index = len(end.Source)
	}

	return Matcho{Source: m.Source[start.Index:end.Index]}
}

func (m Matcho) SliceCombine(beforeCount int, insert string, afterCount int) Matcho {
	start := m.Index - beforeCount
	end := m.Index + afterCount
	if start < 0 {
		start = 0
	}
	if end > len(m.Source) {
		end = len(m.Source)
	}
	return JoinBytes(m.Source[start:m.Index], []byte(insert), m.Source[m.Index:end])
}

func (m Matcho) Rest() []byte {
	return m.Source[m.Index:]
}

func (m Matcho) LimitEndPos(to Matcho) Matcho {
	return Matcho{Source: m.Source[:to.Pos()], Index: m.Index}
}

func (m Matcho) LimitEnd(to string) Matcho {
	return Matcho{Source: m.Source[:m.Before(to).Pos()], Index: m.Index}
}

func (m Matcho) LimitStartPos(start Matcho) Matcho {
	return Matcho{Source: m.Source[start.Pos():]}
}

//returns slice [current:]
func (m Matcho) SliceTail() Matcho {
	if m.Eof() {
		return Matcho{}.Fail("Eof while Slicing. Returned zero")
	}
	return Of(m.Source[m.Pos():])
}

//returns slice [:current]
func (m Matcho) SliceHead() Matcho {
	if m.Eof() {
		return m.Fail("Eof while Slicing. Returned self")
	}
	return Of(m.Source[:m.Pos()])
}

//Makes copy with replaced next occurrence. Places position after replacement
func (m Matcho) Replace(old, new string) Matcho {
	st := m.Before(old)
	end := m.Move(len(old))
	return Join(st.SliceHead(), OfStr(new), end.SliceTail()).Move(st.Pos() + len(new))
}

func (m Matcho) Between(start, end string) Matcho {
	st := m.Next(start)
	return m.LimitStartPos(st).LimitEnd(end)
}

func (m Matcho) BetweenBlock(start, end string) Matcho {
	st := m.Next(start)
	mm := st
	count := 1
	for count != 0 {
		mm = mm.NextAny(start, end)
		op := mm.OpStr()
		if op == start {
			count++
		} else if op == end {
			count--
		}
	}
	return m.Cut(st, mm.Move(-1))
}
