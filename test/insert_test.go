package test

import (
	"gitlab.com/iamgoroot/matcho"
	"testing"
)

func TestInsert(t *testing.T) {

	m := matcho.OfStr("insert here1 insert2 here2 where3 $s")
	inserted := m.InsertAll(
		matcho.InsertPoint{m.Next("$s"), "_inserted0"},
		matcho.InsertPoint{m.Next("here1"), "_inserted"},
		matcho.InsertPoint{m.Next("$s"), "_inserted1"},
		matcho.InsertPoint{m.Next("here2"), "_inserted"},
		matcho.InsertPoint{m.Next("$s"), "_inserted2"},
	)
	if inserted.String() != "insert here1_inserted insert2 here2_inserted where3 $s_inserted0_inserted1_inserted2" {
		t.Fatal("missmatch", inserted)
	}

}
