package test

import (
	. "github.com/iamgoroot/djent"
	"github.com/iamgoroot/djent/inspect"
	"testing"
)

func TestMatchoSelectVar(t *testing.T) {
	qs := T(
		Q("context", "context"),
		Q("log", ""),
		Q("jenpkg/fasd_fasdf", "jen"),
	)
	const input = `package pp_pkg
	import (
		context "context"
		"log"
		 jen "jenpkg/fasd_fasdf"
	)
	var name string
	var ctx context.Context
	`
	pkg, q := inspect.Imports([]byte(input))
	if pkg != "pp_pkg" {
		t.Fatal("wrong pkg", pkg)
	}
	for _, v := range q {
		if v != qs.First().Q() {
			t.Fatal("missmatch", v, qs.First())
		}
		qs = qs[1:]
	}

}
