package test

import (
	"encoding/json"
	"fmt"
	"gitlab.com/iamgoroot/matcho"
	_ "net/http/pprof"
	"strconv"
	"testing"
)

//find r-th user adresses
func BenchmarkJsonMatcho(b *testing.B) {
	n := b.N
	usrs, jsn := makeUsers(n)
	for r := 0; r < n; r++ {
		findUser := "user" + strconv.Itoa(r)
		m := matcho.Of(jsn).UntilPass(func(m matcho.Matcho) matcho.Matcho {
			m = m.ErrReset().Next(`"Profile"`)
			name := m.Rewind(`"Name"`)
			name = jsonValOf(name, "Name")
			userFound := name.OpStr() == findUser
			return name.On(userFound, m).On(!userFound, m.Fail("Not found yet. current user: ", name.OpStr())) //continue search or err
		})
		m = m.Next("{").Next("Addresses").Between("[", "]").Zoe().Before("{")
		addrs := readAddressArray(m)
		checkAddresses(b, usrs[r], addrs)
	}
}

func readAddressArray(m matcho.Matcho) []Address {
	var addrs []Address
	f := func(m matcho.Matcho) matcho.Matcho {
		adrr, mm := readAddress(m)
		if adrr.ZipCode == "" && adrr.Street == "" {
			return mm.Fail("Done")
		}
		addrs = append(addrs, adrr)
		return mm.OnOk(m.Move(mm.Len()))
	}
	m.UntilFail(f)
	return addrs
}

func readAddress(m matcho.Matcho) (Address, matcho.Matcho) {
	var adrr Address
	mm := m.Between("{", "}").Indent()
	mm = jsonValOf(mm, "Street").Indent().Skip(",")
	adrr.Street = mm.OpStr()
	mm = jsonValOf(mm, "ZipCode")
	adrr.ZipCode = mm.OpStr()
	return adrr, mm
}

func jsonValOf(m matcho.Matcho, key string) matcho.Matcho {
	return readQuoted(m.Next(`"` + key + `"`).SpMb().ExpectRaw(":").SpMb())
}

func readQuoted(m matcho.Matcho) matcho.Matcho {
	return m.Indent().ExpectRaw(`"`).Select(`"`).Move(1)
}

func BenchmarkJsonUnmarshall(b *testing.B) {
	usrs, jsn := makeUsers(b.N)
	for r := 0; r < b.N; r++ {
		var res []User
		json.Unmarshal(jsn, &res)
		checkAddresses(b, usrs[r], res[r].Profile.Addresses)
	}
}

func checkAddresses(b *testing.B, u User, addr []Address) {
	for i, a := range addr {
		if u.Profile.Addresses[i] != a {
			b.Fatal("addr missmatcho", u, a)
		}
	}
}

func makeUsers(n int) ([]User, []byte) {
	var users []User
	for i := 0; i < n; i++ {
		testUser := User{fmt.Sprintf("%s%d", "user", i),
			Profile{fmt.Sprintf("%s%d%s", "user", i, "@email.com"),
				[]Address{{"Some srt", "ZIP0003"}, {"Some other srt", fmt.Sprintf("%s%d", "ZIP0000_", i)}},
			}}

		users = append(users, testUser)
	}
	body, err := json.MarshalIndent(users, "", "")
	if err != nil {
		panic(err)
	}
	return users, body
}

type User struct {
	Name    string
	Profile Profile
}

type Profile struct {
	Email     string
	Addresses []Address
}

type Address struct {
	Street  string
	ZipCode string
}
