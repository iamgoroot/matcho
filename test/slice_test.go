package test

import (
	"gitlab.com/iamgoroot/matcho"
	"testing"
)

func TestMatcho_BetweenBlock(t *testing.T) {
	text := `block======={start{ fasdf }{  dsfASD}{ sadf }end}-----------`
	r := matcho.OfStr(text).BetweenBlock("{", "}").String()
	if r != "start{ fasdf }{  dsfASD}{ sadf }end" {
		t.Fatal(r , " != start{ fasdf }{  dsfASD}{ sadf }end")
	}
}
