package test

import (
	"gitlab.com/iamgoroot/matcho"
	"testing"
)

var results = []int{4, 6, 7}

func checkPos(t *testing.T, msg string, m matcho.Matcho) {
	if m.Pos() != results[0] {
		t.Fatal(msg, results[0], m.CurrentLineDebug(), m)
	}
	results = results[1:]
}

func TestExample1(t *testing.T) {
	matcho.DebugOnPort(59456)
	m := matcho.OfStr("qwe(me)")
	m = m.Next("(")
	checkPos(t, "Next failed", m)
	m = m.ExpectRaw("me")
	checkPos(t, "Maybe failed", m)
	m = m.ExpectRune(')')
	checkPos(t, "ExpectRune failed", m)
}

func TestExample2(t *testing.T) {
	m := matcho.OfStr("fsdfdsfqwe(me)fdsfsfd {123}")
	s := m.Between("{", "}").String()
	if s != "123" {
		t.Fatal("Between failed", s)
	}
}

func TestExample3(t *testing.T) {
	input := "var name"
	m := matcho.OfStr(input)
	v1 := m.SelectVal("")
	m2 := m.SelectVar("").Indent()
	if v1 != m2.OpStr() {
		t.Fatalf("variables missmatch v1[%v] v2[%v]", v1, m2.OpStr())
	}
	var name string
	m3 := m2.SelectVarInto("", &name)

	if name != "name" {
		t.Fatalf("name not parsed m[%v] name[%v] pos[%v]", m3, name, m3.Pos())
	}
	if m3.Pos() != len(input) {
		t.Fatalf("final position missmatch [%v] pos[%v] len[%v]", m3, m3.Pos(), len(input))
	}
}
