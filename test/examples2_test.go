package test

import (
	"testing"

	"gitlab.com/iamgoroot/matcho"
)

func TestExample4(t *testing.T) {
	//Parse TODO list
	todo := `TODO:
	Dentist
	Buy cake
	Car wash
	Expect Unexpected`
	m := matcho.OfStr(todo).ExpectRaw("TODO:").Indent()
	var items [4]string
	i := -1

	m.UntilEof(func(m matcho.Matcho) matcho.Matcho {
		i++
		return m.Indent().SelectVarUntil(&items[i], "\n")
	})

	checkTodo(t, items)
}

func checkTodo(t *testing.T, res [4]string) {
	if res[0] != "Dentist" {
		t.Fatal("failed parsing TODO list")
	}
	if res[1] != "Buy cake" {
		t.Fatal("failed parsing TODO list")
	}
	if res[2] != "Car wash" {
		t.Fatal("failed parsing TODO list")
	}
	if res[3] != "Expect Unexpected" {
		t.Fatal("failed parsing TODO list")
	}
}

func TestExample5(t *testing.T) {
	//parse badly formatted properties
	s := `state			= 					   Running
					otherhost=github.com
		port =11111
							host= localhost`
	m := matcho.OfStr(s)
	//if state is not "Running" - do nothing
	m = readProperty(m, "state").ExpectRaw("Running").Zoe().OnOk(m)
	//.OpStr() returns last selected value
	host := readProperty(m, "host").OpStr()
	port := readProperty(m, "port").OpStr()
	if host != "localhost" && port != "11111" {
		t.Fatal("Failed to parse props")
	}
}

func readProperty(m matcho.Matcho, name string) matcho.Matcho {
	return m.UntilPass(func(m matcho.Matcho) matcho.Matcho {
		//Expect Bad formatting. Zero on error - returns zero renders further calls void if missed exoected
		start := m.Next(name)
		//select variable after =
		v := start.Indent().ExpectRaw("=").Indent().Select("\n")
		//check if it was start of a line and line starts with indent and name
		v = v.MoveTo(start).Rewind("\n").Indent().ExpectRaw(name)
		return v.OnErrStep(start)
	})
}
