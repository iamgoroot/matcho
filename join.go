package matcho

import "bytes"

func Join(matchos ...Matcho) Matcho {
	buf := bytes.Buffer{}
	for _, value := range matchos {
		buf.Write(value.Source)
	}
	return Matcho{Source: buf.Bytes()}
}

func JoinBytes(b ...[]byte) Matcho {
	buf := bytes.Buffer{}
	for _, value := range b {
		buf.Write(value)
	}
	return Matcho{Source: buf.Bytes()}
}
