package matcho

func (m Matcho) LookbackExpect(str string) Matcho {
	if m.Pos() < len(str) {
		return m.Fail("eof while looking back for expected string")
	}
	back := m.Slice(len(str), 0).String()
	return m.On(back == str, m.Fail("Looked back. expected:", str, "got:", back))
}
