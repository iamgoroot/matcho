package matcho

func (m Matcho) HandleErr(handle func(self Matcho) error) Matcho {
	if m.ErrorStr != "" {
		handle(m)
	}
	return m
}

func (m Matcho) HandleOk(handle func(self Matcho) error) Matcho {
	if m.ErrorStr == "" {
		handle(m)
	}
	return m
}
