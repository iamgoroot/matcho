package reference

import (
	"testing"

	. "gitlab.com/iamgoroot/matcho"
)

type MatchoList interface {
	Nav
	NavString
	Select
	Expect
	On
	Op
	Handle
	Error
	Slicing
	Skip
	Looping
	Debug
	Reset() Matcho //Zero
	String() string
}

type Nav interface {
	Pos() int
	Len() int
	Move(off int) Matcho
	MoveTo(to Matcho) Matcho
}

type NavString interface {
	Before(token string) Matcho
	Next(after string) Matcho
	Rewind(until string) Matcho
}

type On interface {
	On(condition bool, matcho Matcho) Matcho

	OnOk(matcho Matcho) Matcho
	OnOkCall(f func(Matcho) Matcho) Matcho
	OnOkOpStr(op *string) Matcho

	OnErr(matcho Matcho) Matcho
	OnEof(matcho Matcho) Matcho
	OnErrStep(matcho Matcho) Matcho
	OnErrCall(f func(Matcho) Matcho) Matcho
}

type Select interface {
	Between(string, string) Matcho
	Select(until string) Matcho
	SelectVar(allowed string) Matcho
	SelectVal(allowed string) string
	SelectVarFunc(f func(r rune) bool) Matcho
	SelectVarInto(allowed string, v *string) Matcho
	SelectVarUntil(v *string, until string) Matcho
}

type Error interface {
	//Zoe - Zero on error. disable further operations
	Zoe() Matcho
	//End of file
	Eof() bool
	//Panic on error
	Pof() Matcho

	Err() string
	ErrSet(prt *string) Matcho
	ErrReset() Matcho

	Fail(err ...string) Matcho
	FailEof(err ...string) Matcho
}

type Slicing interface {
	Slice(beforeCount, afterCount int) Matcho
	SliceCombine(beforeCount int, insert string, afterCount int) Matcho
	LimitEndPos(to Matcho) Matcho
	LimitEnd(to string) Matcho
	LimitStartPos(start Matcho) Matcho
	SliceTail() Matcho
	SliceHead() Matcho
	Rest() []byte
}

type Allocating interface {
	Replace(old, new string) Matcho
	ReplaceAll(old, new string) Matcho
	Insert(s string) Matcho
	InsertAll(points ...InsertPoint) Matcho
	InsertPoint(s string) InsertPoint
}

type Debug interface {
	Dbg() Matcho
	CurrentLine() string
	CurrentLineDebug() string
	StringDebug() string
}

type Handle interface {
	HandleErr(handle func(self Matcho) error) Matcho
	HandleOk(handle func(self Matcho) error) Matcho
}

type Op interface {
	Op() Matcho
	OpSet([]byte) Matcho
	OpStr() string
	OpBytes() []byte
}

type Skip interface {
	Sp() Matcho
	SpMb() Matcho
	Indent() Matcho //SpLn ?
	Skip(anyOf string) Matcho
	SkipSeq(tokens ...string) Matcho
}

type Expect interface {
	ExpectRune(r rune) Matcho
	ExpectRaw(raw string) Matcho
	ExpectNot(raw ...string) Matcho
	Maybe(raw string) Matcho
}

type Looping interface {
	UntilPass(repeat func(m Matcho) Matcho) Matcho
	UntilFail(repeat func(m Matcho) Matcho) Matcho
	UntilEof(repeat func(m Matcho) Matcho) Matcho
}

func TestInterface(t *testing.T) {
	var matcho MatchoList
	matcho = OfStr("")
	matcho.Rewind("")
}
