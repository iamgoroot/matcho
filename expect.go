package matcho

import (
	"strconv"
)

func (m Matcho) ExpectRune(r rune) Matcho {
	if m.Eof() {
		return m.Fail("eof", m.Err(), strconv.Itoa(m.Index))
	}
	next := rune(m.Source[m.Index])
	if next != r {
		return m.Fail("not found rune")
	}
	return m.Move(1)
}

//Set Op on match
func (m Matcho) Maybe(raw string) Matcho {
	got := m.Slice(0, len(raw)).String()
	if got != raw {
		return m
	}
	return m.OpSet([]byte(raw)).Move(len(raw))
}

func (m Matcho) ExpectRaw(raw string) Matcho {
	l := len(raw)
	got := m.Slice(0, l).String()
	if got == raw {
		return m.Move(l)
	}
	return m.Fail("not found raw. got:", got, "expected:", raw)
}

func (m Matcho) ExpectNot(raw ...string) Matcho {
	l := len(raw)
	got := m.Slice(0, l).String()
	for _, r := range raw {
		if got == r {
			return m.Fail("not expected:", got)
		}
	}
	return m.Move(l)

}
